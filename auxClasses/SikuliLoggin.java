package auxClasses;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Region;
import org.sikuli.script.Screen;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;

import auxClasses.Browsers;
import auxClasses.ReadUrlFile;

public class SikuliLoggin extends Browsers{
	//Define screen for sikuli.
			Screen screen;
			
			
			@BeforeClass
			public void log() {
				screen = new Screen();
				Region region = null;
				
				try {
					//ingresamos a la pagina
					driver.get("http://compras135.ufm.edu/");
					screen.wait("img/IngresarUFM.png").click();
					//ingresamos el usuario
					screen.wait("img/user.png").type("xik");
					screen.wait("img/Next.png").click();
					//ingresamos la contraseņa
					ReadUrlFile.waitForLoad(driver);
					region = screen.wait("img/PasswordRegion.png");
					region = region.find("img/Password.png");
					region.click();
					region.type("xik$2015");				
					screen.wait("img/Next.png").click();
					//dejamos un tiempo para verificar que se ingreso correctamente
					ReadUrlFile.waitForLoad(driver);
					ReadUrlFile.Wait(3000);
					ReadUrlFile.waitForLoad(driver);
				} catch (FindFailed e) {
					System.out.println(e.getMessage());
					Assert.fail(e.getMessage());
				} catch (Exception e1) {
					Assert.fail(e1.getMessage());
				}
			}

}

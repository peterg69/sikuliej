package sikuliej;

import org.sikuli.script.Button;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Region;
import org.sikuli.script.Screen;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import auxClasses.Browsers;
import auxClasses.ReadUrlFile;
import auxClasses.SikuliLoggin;

public class SIkuliEmulacion extends Browsers{
	
	@BeforeClass
	public void setUp(){
		SikuliLoggin l = new SikuliLoggin();
		l.log(); 
	}
	
	@Test
	public void emularCatedraticoSubirArchivoAUnCursoSikuli(){
		Screen screen = new Screen();
		Region region = null;
		//accedemos a MiU
		driver.get("http://compras135.ufm.edu/MiU");
		ReadUrlFile.waitForLoad(driver);
		
		try{
			//Hacemos Login en MiU
			screen.wait("img/MiULog.png").click();
			ReadUrlFile.waitForLoad(driver);
			//Nos dirigimos a soporte al estudiante
			screen.wait("img/Recursos.png").click();
			screen.wait("img/SoporteAEstudiantes.png").click();
			ReadUrlFile.waitForLoad(driver);
			//emulamos al catedratico
			screen.wait("img/UsuarioAEmular.png").click();
			screen.type("20080633");			
			screen.wait("img/UsuarioDanielMauricio.png", 5).click();
			screen.wait("img/Emular.png").click();
			ReadUrlFile.waitForLoad(driver);
			//Nos dirigimos a cursos actuales
			screen.wait("img/MisCursos.png").click();
			screen.wait("img/CursosActuales.png").click();
			ReadUrlFile.waitForLoad(driver);
			//guardamos referencia a la ventana original
			String parentWin = driver.getWindowHandle();
			//Seleccionamos el curso donde subiremos el archivo
			region = screen.wait("img/CursoRegion.png");
			region = region.find("img/Curso.png");
			region.click();
			ReadUrlFile.waitForLoad(driver);
			//cambiamos el driver a la nueva ventana
			for(String childWin : driver.getWindowHandles()){
				driver.switchTo().window(childWin);
			}
			//elegimos la opcion para subir el archivo
			ReadUrlFile.waitForLoad(driver);
			screen.wait("img/AgregarArchivoLink.png").click();
			//llenamos un poco de informacion
			region = screen.wait("img/DocumentoTituloRegion.png");
			region = region.find("img/DocumentoTitulo.png");
			region.click();
			screen.type("SikuliTest");
			screen.wait("img/DocumentoDescripcion.png").click();
			screen.type("Esto es un archivo de prueba.");
			//abrimos el explorador de archivos
			screen.wait("img/DocumentoArchivo.png").click();
			//cargamos el archivo que queremos subir
			screen.wait("img/Archivo.png").click();
			screen.wait("img/Open.png").click();
			ReadUrlFile.waitForLoad(driver);
			//hacemos un scroll down hasta que la opcion de subir sea visible
			screen.wheel(Button.WHEEL_DOWN, 10);
			//subimos el archivo
			screen.wait("img/SubirDocumentos.png").click();
			ReadUrlFile.waitForLoad(driver);
			//hacemos un scroll down hasta que el archivo que subimos este visible
			ReadUrlFile.Wait(2000);
			screen.wheel(Button.WHEEL_DOWN, 10);
			//Damos un tiempo para corroborar que el archivo fue subido correctamente
			ReadUrlFile.Wait(5000);
			//cerramos la ventana actual
			screen.wait("img/X.png").click();
			//regresamos el driver a la ventana original
			driver.switchTo().window(parentWin);
			//terminamos la emulacion
			screen.wait("img/TerminarEmulacion.png").click();
			ReadUrlFile.waitForLoad(driver);			
			ReadUrlFile.Wait(1000);
		} catch (FindFailed e) {
			System.out.println(e.getMessage());
			Assert.fail(e.getMessage());
		} catch (Exception e1) {
			Assert.fail(e1.getMessage());
		}
				
	}

}

package sikuliej;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Region;
import org.sikuli.script.Screen;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import auxClasses.Browsers;
import auxClasses.ReadUrlFile;
import auxClasses.SikuliLoggin;

public class SikuliFormulario extends Browsers{
	
	@BeforeClass
	public void setUp(){
		SikuliLoggin l = new SikuliLoggin();
		l.log(); 
	} 
	
	@Test
	public void crearNuevaSolicitudDeLOGOSikuli(){
		Screen screen = new Screen();
		Region region = null;
		//nos ubicamos en la pagina correcta luego de iniciar sesion
		driver.get("http://compras135.ufm.edu/adm_academico_select_facultad.php?action=adm_academico_admin_facultades.php&changeTitle=true");
		ReadUrlFile.waitForLoad(driver);
		
		try{
			//nos dirigimos a la administracion de LOGOS
			screen.wait("img/CiclosAcademicoss.png").click();	
			screen.wait("img/AdministracionDeLOGOS.png").click();
			ReadUrlFile.waitForLoad(driver);
			//oprimimos "Nueva Solicitud"
			screen.wait("img/NuevaSolicitud.png").click();
			ReadUrlFile.waitForLoad(driver);
			//llenamos el formulario			
			//nombre y carn�
			screen.wait("img/Nombre.png").click();
			screen.wait("img/Nombre.png").type("Karl Heinz");
			screen.wait("img/NombreAC.png").click();
			//Categoria
			screen.wait("img/Categoria.png").click();
			screen.wait("img/Catedratico.png").click();			
			//Titulo
			region = screen.wait("img/RegionTitulo.png");
			region.find("img/TituloOpciones.png").click();
			screen.wait("img/NuevoLogo.png").click();
			region = region.find("img/Titulo.png");
			region.click();
			region.type("TestXIK_Gonzalez");
			//Descripcion
			screen.wait("img/Descripcion.png").click();
			screen.wait("img/Descripcion.png").type("Esto es una prueba");
			//Cantidad de LOGOS
			screen.wait("img/Cantidad.png").click();
			screen.wait("img/Cantidad.png").type("14");
			//Reposicion
			screen.wait("img/Reposicion.png").click();
			//Ciclo
			screen.wait("img/Ciclo.png").click();
			screen.wait("img/Cualquiera.png").click();
			//Cupo
			screen.wait("img/Cupo.png").click();
			screen.wait("img/Cupo.png").type("69");
			//Seccion
			screen.wait("img/Seccion.png").click();
			screen.wait("img/Cupo.png").type("A");
			//mandamos la solicitud
			screen.wait("img/EnviarSolicitud.png").click();
			ReadUrlFile.waitForLoad(driver);
			//cerramos la ventana
			screen.wait("img/X.png").click();
			//Buscamos la solicitud
			region = screen.wait("img/BuscarTituloRegion.png");
			region.find("img/BuscarTitulo.png");
			region.click();
			region.type("TestXIK_Gonzalez");
			screen.wait("img/Buscar.png").click();
			//Esperamos para corroborar que la solicitud se envio de forma correcta
			ReadUrlFile.Wait(5000);
		} catch (FindFailed e) {
			System.out.println(e.getMessage());
			Assert.fail(e.getMessage());
		} catch (Exception e1) {
			Assert.fail(e1.getMessage());
		}
		
	}
	
}
